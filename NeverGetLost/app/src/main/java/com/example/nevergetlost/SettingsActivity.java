package com.example.nevergetlost;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsActivity extends AppCompatActivity {

    private EditText EdittextPlace;
    private EditText EdittextDate;
    private EditText EdittextTime;


    public static final String SHARED_PREFS = "sharedPrefs";

    private String text;

    private static final String tag = "SettingsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        EdittextPlace = findViewById(R.id.edittextPlace);
        EdittextDate =  findViewById(R.id.edittextDate);
        EdittextTime = findViewById(R.id.edittextTime);

    }

    public void saveData(View view) {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("Place", EdittextPlace.getText().toString());
        editor.putString("Date", EdittextDate.getText().toString());
        editor.putString("Time", EdittextTime.getText().toString());

        editor.apply();

        Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
    }

    public void displayData(View view) {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String date =sharedPreferences.getString("Date","");
        String time =sharedPreferences.getString("Time","");
        String dateAndTime=date+" "+time;
        Snackbar.make(view,dateAndTime,Snackbar.LENGTH_LONG).setAction("Action",null).show();
    }


    public void onStart()
    {
        super.onStart();
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String place =sharedPreferences.getString("Place","");
        String date =sharedPreferences.getString("Date","");
        String time =sharedPreferences.getString("Time","");

        EdittextPlace.setText(place);
        EdittextDate.setText(date);
        EdittextTime.setText(time);
        Log.d(tag,"In the onStart() event");
    }

    public void onResume()
    {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String place =sharedPreferences.getString("Place","");
        String date =sharedPreferences.getString("Date","");
        String time =sharedPreferences.getString("Time","");

        EdittextPlace.setText(place);
        EdittextDate.setText(date);
        EdittextTime.setText(time);
        Log.d(tag,"In the onResume() event");
    }


    public void onPause()
    {
        super.onPause();
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("Place", EdittextPlace.getText().toString());
        editor.putString("Date", EdittextDate.getText().toString());
        editor.putString("Time", EdittextTime.getText().toString());

        editor.commit();
        Log.d(tag,"In the onPause() event");
    }

    public void onStop()
    {
        super.onStop();
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("Place", EdittextPlace.getText().toString());
        editor.putString("Date", EdittextDate.getText().toString());
        editor.putString("Time", EdittextTime.getText().toString());

        editor.commit();
        Log.d(tag,"In the onStop() event");
    }


}
