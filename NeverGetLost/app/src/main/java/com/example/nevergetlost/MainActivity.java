package com.example.nevergetlost;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Beautiful Countries to Visit");
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        initImageBitmaps();

    }

    private void initImageBitmaps(){

        mImageUrls.add("https://i.pinimg.com/originals/44/21/5b/44215be63cf24986076bbf2de24666b1.jpg");
        mNames.add("Malta");

        mImageUrls.add("https://www.riotgames.com/darkroom/1440/b2b587d91d3c5d2922953ac62fbb2cb8:dfd0d5c2d07f981fb8cda29623b5e54e/paris.jpg");
        mNames.add("France");

        mImageUrls.add("https://s3.amazonaws.com/images.hamlethub.com/hh20mediafolder/5337/201904/rome-1554318872.jpg");
        mNames.add("Italy");

        mImageUrls.add("https://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1485964748/park-guell-barcelona-spain-DEALSPAIN0117.jpg?itok=zQE6o0B6");
        mNames.add("Spain");

        mImageUrls.add("https://routemate.in/wp-content/uploads/2018/08/munich-germany-europe-holidays-routemate-tourism.jpg");
        mNames.add("Germany");

        mImageUrls.add("http://valueadzvisas.com/blogpic/large/1530614595uk.jpg");
        mNames.add("United Kingdom");

        mImageUrls.add("http://tttravelus.com/wp-content/uploads/2019/03/5-Things-Filipinos-Love-About-Japan-7.jpg");
        mNames.add("Japan");

        mImageUrls.add("https://media.swncdn.com/cms/RT/63883-egypt-pexels.1200w.tn.jpg");
        mNames.add("Egypt");

        mImageUrls.add("data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTExMWFRUXFRgVFxgYFxsVGBcWFhgWFxcXGBoeHSggGholHRYVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy0lHSUtLS0tLS8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALcBEwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAEAAECAwUGBwj/xABCEAABAwIEAggCBwcCBwEBAAABAAIRAyEEEjFBBVEGEyJhcYGRoTLwFBVCUrHB0RYjM2KS4fFygkNTVKLC0uKyB//EABoBAAMBAQEBAAAAAAAAAAAAAAABAgMEBQb/xAApEQACAgEDBAEEAgMAAAAAAAAAAQIRAxIhMQQTQVGRFCJS8DNhQoHR/9oADAMBAAIRAxEAPwAf6KpDDhESlklfQ2eZRSKbQnztCt6gJdU3miwoodV5KPWIgtCbL3IsKByO9O2n3q8A8oUXQnYDNYFItCra0qYpFKwJtYnMJm0VaKKLArCZyuLQoF/cmgKC9VPJKKzd3sptAOyomzNNMlIYU8lrNohRzMDxTkZy0vA/laQCfVwRqoLZnNwZ5KxuFK1MibKjUIBZT7kQxXZFIU0rAZoCmGKOTvTosKHyBMWqDiU7XFFjodNlKuapghLUUkD9QnGFRIIUgVGotIHGHCsDFcCpgdyWoqihtJTFFWFwGpCpq4mmNXD8UbhsWdWEkCeM0RafZJPSx2A9cE4qHkqKbZRNOkgjYk0FS6kKYIG6l1g5pEkBRCkKYSL1AuKAJGmomkFA03HdR+jlMCZc0KJrDZRNEKJaPkJ7CEah5JAlO1qTyQnYh+r70uqUA8qYlFjom2mE+VSYHFXU8MTslqCioLEr5frGlM5voz4uN3Wt4Z/ULpm4U80di+hOGL24kCqXgAh+YCBGkREQYjvK4+q6qOJR1eWbYsTldGWHBOT3IxuFAVgoDkurWZaTODVLqHLQIA5Kh+MYN58Lo1BpKW0SpCioO4k3YEoZ/FXfZb6p2woM6hOaYGtlm1MTVdvHshzQedXFAUapr0x9oKv6xpDn6IClg27lENoMHzKNikh6nFfutkd/9lT9aVDo1o8ZlH0GM0gKb3hv2QAlqQ6M2pjq3OPAIZ9aqbSSD4rbdjLaAqs1B/KPRGr+h0YIoumZTNwh3Pqt1wkbeSqe3uT1joxzgx95JaZpHkmRrCihlJ86wiG4Vx+JxRwYpZVGsnSCNwgVgpKyrVa0gOe1pOgJAJjWJ1T/AEabylrDSUloCTXg6Sim4cK0U0tYaQINJ2TfRDzWgKakGJax6QAYNS+ihFWB1UalQDQSUu4UsT5AqlLu9Aqhhe4oqriCPujzEqp2Obu7y1TWT0DxNcjtw0bqxjGIVuLpEEkxHM/gNVSeIU9g4+aNVh22tzSaWjRSdidlmfXDAI6v1urcFxI1Xtp06ILnGBt4k9wEk+CG/LFpNXBtqVHhjYzGYk8hJJPKAundjWgin9nKAR1jZtb7sT3ShDwsNbGp0JjnGnuY/lQP0ca7Zc3lniPRebnlDPy9kbQUocAGPpVKbix/ZtIMyCDpePyQbXu3J7oJXVDhVOo3KbagEWI1v+BjvXGcQ4TWpVHU3EmDYjRzToQurF1MZKm9yezJvZFtUt315/JVAqMHxO9P1CanwiodbK76lP3vKP7rT6nEttRouiy/iU9dT7yq3YobNPmUZ9UkcvUBMcOAbhUs8HwyJdPOPKAhinbNhSzPOpRRp8grqb+TSSm8qEsTAGs8PVWwW/eWgzDE3FEz3yiW4etsxo8Vm+ph7RoumyP/ABZltzfYb6qbsDVPxEDvsFoO4dWNi5gUxwupEF7Y8SfaFP1MPZX0uT0ZtPANb8T585Un0qfj4I/6rvcz4KbeHDl4XVLMmQ8TXIDNICwMpMZmsYWm3hw5eyuZge5HcQtBjjAlJbowZ+SkjuD0nlrunVN9FwNNzKhDgAHSBIMOzAA6xsuGxGJe52YlxMC5dJ5kzzm/mqcwJuokEEb7+XivPlOUuSlFItrYsus5xdpJcS+YkgXvqu26FdKqbGdVXqPmQGZrtHdmJkbWNhtyXE1KYABkXmwm0RrPP8kqWXNLj480Rm4u0Nqz3FuILhLGzNxcQQe8FSJqW+FvcTK8/wCF9MKdACmzD5qTBYlwD9ZcbkjnA8Lr0oNBaCzQidANV1LMmRoKalCdXHylD1qlOnqT679902J4c5xkuI80IeFsHM+F/wAU9SfkpJrwVVuLs2nyVH1iw/YcfEoulhWzDaYHiR+CLZw5x0Y0IcofrKUpLwvgyH1GHSmW9/xfiVR9BnTN6R+a6ZnD62xa3wF1I8IfvUv6IWWK4YPVLlHMNwB8FaMCN3Bbdbh7GXc4fjKFdXpN0k+Aj8VXdvgjTXIEMC3xXWdBcGM1SpDQGtDQYi5ubxyb7rl6nER9lkea7rDuotoMHaLcmYAkzDmvcZAMSW59uS5eryuMKfk0hT4L8e2m0iS0kO3JB7OV51P8tM+XesB9VsR+7jJrmb/zOszRm+9stOrUpF+UMF3uZJa3UUxUzeEWPegX4unlz9WP4BrxDZ7P2dO9cEcleP35LcP7NHhwYSYySXTY71HF40O7i4+aj0r7NOnUjMB2Sbkw4S3w/uq8PVpZ8pY2z6bZyt1e3OHaaDTxWpghRqUyAC1rmQRpDXN0gWBAj5CjuVJNrYqNx3TOJZxXkwnzKapj6h0bl8T/AGQFTEiTlPZkxzjaYtMKIxS9VYorehvqJtU2G53nV6m2gCbkn58Vn/TTsE/1i/mB5BXT8EavZsUacaD2B/FGsrP5x6Bcu7GPP2ioGs47lQ8afNFLJJcNnXfSOb/dQOJZ96fOVycuUgHJqEVwDcnydU3GsG4UhxFi5TKVIA80bC0HUjiY5KQ4mByXKwefumjvRaDtnW/W9PchP9c0vvey44hRIT2Dts7L6+pcymXGpIpB2zzKtwuqMw6oksEuyw6LkXLZGxGqGB1FvXXvV2F4g9s5HOZIyktOWRa0i8WuhCOR3XIZEwJj5Ck9haYO2xn+yrZTJnuud+QH4qQeTpMxB3PlyQOg/hPGKmHdLMhEEFrmhwIOoK9n4BxnC1KdJtEsbNMFtKRma0WjKCdF4M2/dCuw7y0hwLmkXBBLSJEGCLiyNxp0fQ1Qg7eyErNaNifEgD8VxnQXpm99YUcW7O18NpuIuHkgNaQxuhk9o6QF6DiKmHHxGl/UP1WEs0oumjrxwhNWmYj8VFmBo82KipXrwf3wA5BwH4NlaNetg+QJ7pKCfisMNGH581rHO/xfwTLAvaMx1SsdXvP+9xTCiftOd7/mjK2Op/Ya4eiCqYknmt45ZPxRi8cV5sJa+kB/Dc7xMfmo/S2j4aLfOSgzVPNRLiqS9k36NnhFJ+IqdWxtNlpLi0mB+Z7l3v1a0Ma1wnKwNJPZmG5ZymI1Nr6rz7owXdfYkdk3BgxIsu2wuGblEjMeZMn/AId/O/qV5/WZHGSS4NMcdXI2JwbJJzMBBLvs2LgGk2PKAgX8PpaZ6fw5Yn7PL4tO7Ra1fDsvDW6EacnMA+e9JnVOeYY2A8NmBBGUyBzbdcn1ORcfvH/TTtRM6jhKYM56cyD9nVkgG5208lq4HDtEZQCAIsRoPAn8OSGfSZA7LdHjTYu/BQxWGZLuyB226cw5g9gAh55PkO0vBxPSHgrsK5ozZ2uEg5S021BBWVmW90kp3pAkmGECSTv3+AWN1Y5L2sOa4Jy5MXid7FcqSn1Y5J8o5K3kRSxkApCVIeCm09yl5C1BEACpBhVrXDklUxTG/EQ3e5Ass3lZqscSIpFSGHXI8X6Z1G1C2i1ga22Y9vNpexgDuuiuGdOWF2WszJazmy4TuC3Ue6h5JCU8V0dQ3CqQwZVNXi9JjabnPDW1fhJ00m52VvDuKU6wcadQOyuLTbcb+B2O6zeWR0xhC6H+h+CmMCiGYtgdkNUB1uzab6W8x6ow1IEuc4DvgBYy6iSN44Iszfq48j6FJa1v5vZJZ/VMv6Zej5+FOPx1FvFXMpSKhAb2Whxl0QJAzN0k3iL68wruKmrV+INMXtYxAEAaACNAEAcG4HtNNt9fddp8/sVvcrcJiHNJykiRBgxI5GNlTViIv4KITAKc7dSoNL7A38FTVdeFPCuLDmbEgjWdQZ2QI2aPR6uIcGPcMxByAuLcrQ8nMJEQ5sHv7l7Bg+jNKpSY8GpTzMa7K6CWyAYPevLML0pxYYerqua0QSGunLoBrJgcjIMm15XpfCum1N9Jjng5y0ZoygZt4BdIG64+oefbQvg7OneLfU/kvf0OO1Yebf7ql/Q+qNKjD6/oix0vpcnerP8A2TftTIBY0GdJzD3DT7LGMut9fNG0n0y/WZbujeIH2Z/3D80DiMG5g7Zju39F0FbpHUNm5W8zle4+XZj1WJiWmo4uqVHEkySA8ewaAuzAs7/kpHLmyY1/GmUtwby0ODXZSJBgwQdCm+ju5FaGF4hWY0NFd0ABrR1ZMAd+SfxU24+q/V4JJ5vaT5CI8Fajlvdqv9h3cdcMv6L4VoqZ3uDREXNySRtysuyFag0a5osdbTGvL4R6LgcTXc2bMzGD2s7j7mUFX49Tae09s3H8J32SQfcFKWK95Mzc23sj0epi6B1gzOt7HXdU1MThtcrZgt0Hw8tdLrgMRx5jfie3cfwnbGD7obF9JaTHhjntmT/wnWtffeAEuzEWtnoLsXQAs0aZbDbl8Wienj6LrGRod7wZGk8guFoccYQcr2xof3TuTj+DXeijh+OMdORwvA/guuSCQPYp9qAapHQ9IaVNxbkdOWWkGQee4voVj/R02AeXtEinIH3HDu1lW4quWj7NiJim6Ym8X703jaX2s1x5ktpIiMOeSIweBdU0Eb3gTp/7BCPrhws548KZ/wAqumzL8Lq3o/8AVKOObW7Rc+ojf2ILbRPJTFHuVLMQ4bvI5Fk7c9dVYMeQCXBxi9mEW77lZSx5fFHRHqcPmyTmACTAHMrzXpVxhlWtNMkgDIDpm1kxrFzrzPNdXx7pUwMygCXggg9vskEEfCI1XnmMxRdeAB2RDbCGiGj0H5p4oz5kjLqc0JLTBlbp1N/m6ZroGgVdQTEfNlDMRE+HO9jb1W9HCFdc6MuYxqRNp1mEqFUjRxHt+Gu6jTcIO5uTtawn3VZiQTaI7xpf570UAVVxT8weT2xBa4m9rg81c7i9R0Ne9zwXF0Fx1IAIN5AIjQhUYikCWumRlm025NBvIGx7lW8gAAXO5t8hJxQ9TXDOlpdPcU0BuZtrfC30+FJccSToD+KdZ9iHpG31GX8n8mhRrPmBYbSbG4GqJpkjY7eU73QdZ1xOptvPoVNtYFvte/LadFqc9AuPbDrmZv6qLGgjn+KniaDiZAJAGul9/JMQ5uoifKfJMZU+kZsPNXN5JCpa419VMNnmAN+9MArg9IuqNa3Lm17TsogETP6dy6EUnh7mhzWtzxJZMCDyHMAW5rkiI8RfyWjR467RwntCSLGBM+dxHgjwCN4CqY7bLuM/uxYCYm2/cux4ZSPU0zmuWMJkw2YE223XNUeqc2m4ZrlxdrqZyRyEFq3OIY1tDBhxgkMaGgyAXRbxjWN4RGXJTibIpNuMxnbtfhuoCmJInYWBMj3XAdHOk9br2MrPztd2ZIALSfhdIA8PPuXdUXDOTIggADvGqq0QRfTME5xMnLewB0zXuUZTaMl7jKZjfWYVVSqxwaCBAADspibkyTGt/ZENsyw7Mc7xPgpjO3uinEppsbkblBDQLTfcbrExWGaXXH26nvUfqugYJpgtHZtBnb0WbxXBAuZ2iztHQB2ZziXb6Xn1RaaCmmU4qgJ01Lx6uMrz7poxoxdTK4kyM38pjQd0QfNel8TwmYshxZc6AOkujXNpvpzXI9Jui/WYwuBdFTIXaQCalOnA7gzMfFqdpMVNnQ4AipSa4EuAaxoJEEtbSeG+x35q6jTJyyCILAJbl7LWvDSOYg2O6MwOBdTYGvJcGsY1mZoaQxrC1sZQJsdSnwmCqNjrC53ZZlzNDeyAQ0jKBMgrONfbx++i3e5PA1jTaHNBkchJ9IO07KPE4dSJ27Jv/qBVmGou6sWJm0DUzso49p6o25fiFcZK7JcWkZgym7Tl0F+1prtHsieraC0TFzbWbc9rlVPqsJJFMBuzYzAHLE3HmpPxjGAueBAbcn7OhnT5lCntwDjuW9SM0ZhGX4f5pnNm102Q9R9IA56jSQTuGxMw2B6d64jifSuoajnU4AjK1waS4NmdTaTHLfzWFUD3nPJOtzO6psngM6RPa5wdTzOsQ6RG5ygbkRrOiyKdFzzeRsBv5c9kYxr5uYgGdPZRw5JdLcoNvivFjYTz/JZpUqKbt2UVWgZYaQ4mJNvOJ71RTpnNOWYuRMf7Z8vZaGMDzrlMbanvI3GyEbiDZrbTqSZKadiJHCNBv5dw8VVXp9q5tqOXeisoOYgy4crc++58EO/NlE3vvPzaR6JiK2Pgzt896J6oltxHj86JU4az4RMXJi99PD52UKmIzG99NPdIY7cLIkA+qSk+m6f1j9EkAU1SfIflzSw+Jjaf1/yin4SAS7MTpc6QOSBqtGylOx0aDcY3dpuP8x7qT2seCBZ0QDMyBsfUrNYCRCvw32mzB2JTFRS8ZDDr7jkpMqGbXCix2Y6CYjnomgHW0JgEspA2LdtOQQbwBpz39kVcR2tFTWEGxsgDp+juNLJbUuCL7wQIBnUR2dOUaTN/SfjHXtpsAADLSN7ADYRaO+ZWAK0ReOyImddr81GtVEid72TSC3VD4akesYNZe0bjcbr0irxYU6ha5hmG6eC4XgQmq11uxDoiScpBiPAG+gXRydXAlx1KBqzTZ0gAzMa0S45vhPZHIEz6rW4Zxc1T1fVAdkmzjoCP5RzXJl4c4uDMtg2xMGLTfmZWjw+m9zg5rywN7DrTmFVrwB3XbKSihuTo6WlWxIpNYOpDYAaHMM//ALk2j0QtZuIeWl/VSDLQWvaZbaYD7qylh67qoyuOUBoy5TftdqD3jKIQ2NqO+kEuBltVtgDZgbdsEi8nUAfmRRp7IHJtbsLqVnmC/Lnb+8icpEH4y3l3aLnq1PFmq6t9IZmNgRTDoaCSBr335rTptccS+oRYtAZIAeJERI2m+pXQFgnUq0l6M237MilisS4Zs7XSGU/h7ILRBsCCAdbkxZWPr4w5e3SIgNkUzAazYnrLb3Q9KpVFTEAveW2bT3yHLJIBGkkGB3ojA0a5Y/rHF7HtyCxhr8pzQdRJc4wOaz001si1K1dluGxmJDGw+m9sgtyMsdpnrFkYzpIQXsNPR2U35HwRmGw1SnScwFx7bnAkaAmcott+a5yrhjTLqbjnc1xBd8JcfvRJ18SmoJLgbm3ywt/TIBxIpCQCwACGjM3Lcan4jr4Ss/8AbMmhXouptis3IdSQADB8QbyRyssDjVHK49i7hM+EybeSyHPnUHx5qdEaoHOTYV1bTefdVF5cQB/bQ+2ihRP3bjvRLaAiXODSdo8bm6tuiAjEVW5YgOOk/wCm0E+34IGm5o+HxtfzuNiSNURVc1jezBd94gQBvY2myHByjtesSOe26hAO+m+SRIYcukAEnXvgE+yzKzzmJOs+vf8AgtfMbgGIgHxnYageCzcVRAkl0m28+/5KkNCwpBkuNvHf891c+1jOvsLx7+6qp1wKcQJ35qPXx4g+40KAC8TUPtJ53n58lThwIzFs8vnT/Ck9rnmQDcDLqdfk+hRGFw3Y/eMJN47Ua6R+F/RIAR9UT/dJa/UU9qdPzE+6dKwM/EcRcW737vKfnkq6mHdkbAkxJ530RXGOE1KLgIcReIExBuLeIQ2GxNRhmHHbQp6dOw7sG6siQQQfRTw1B7muLGF0W8PmVq1Me4tJNICxuSZ05bofhdVwpZRETr5j9ITFYHUwb6ZAc0Xg/wBWg8bJnYJ+UPDZBcWxeQ6Ygjbkts1XHUN1nQ6jTfvKlTBAi3Pn+JumLUjD6io1zQ5pvp87QpYHBOq1HMLmtIk3k6GCIC3xU7m/0hMImYvrN+UfggWpCw/R97pGYQ2GnvIm8bCIEfylVVejpLiBWpCAPid+fzqpvg6tB8bpw1v3QPJFhqC6mBoU20YfLo/eZSSA4EGWkHQi0EfhY1/EqeUgTMGOydYsslo+YT2SsNZosxtICL2GsHX0S4fx2sHFre1TkuaIiABmzEnuc3fcIPh7Guq02u+EvaHf6ZE+0rsKvBMCYiASQNRMei0h7ByMij//AEGNaYPgXj/wWHj8cytWdWLntc68ND7EMa0XyC0gyr8dh8lV7ADDXEDXTZFcFNLPNVrnsEyGnKdDEEjnCiU2Nb7UD8ExINTC0wanYqEknNlvBiI0gHwvzXoGLxThcQPW/qAsLgHC3089YtIDgAy0kg3kw4QPhP8Ai+m15qAlrrBxYRkPZLbWvobFaR4IkjlukeJa2tTLzUEHP2ZggwDEaOgBCcH6Rsw2cUxIdHxh+xfFo5OA/wBq67jvDalWlALHkFrmtIc0yJkTm7NovvJ0XFYrDOb2Q1xcLOsSA4TIB8rHvSkmnY4vY2G9OZPbDcvJrXg6cyOazOM8dNZ3WUeyAGjtaktmbEydVQ9pn+HUi0mL3A2i0H2XVcF4Hhn0WuqtaHkAkOjW9jIQrfI7owH8UZIOUyNYAMgi41QuKxNFzYbSLTmBnKBo4Hn3FHcdw7KNcimBkAaW27Jtfxus0DvJUPYeos43h6Napnp1G0hka0tyEdoG7vMH2Qr+EhozCq18QAGyHXIHOeVlYWhMGBTYtRlcYpvYWl32tuWX8dULVqRBOnIR6kT8wt2tgmPILpMd6qxPDKZvBmZiY8QgNSMZtYu3idoAEbifPRQxDTAOt48wB+MorD4W9IHeSeUXK1ixsBoayBoIGus80xt0Yb8G6M2XK3SBc9mxNzuQT57aKL8M6C7KSBd3da3vK3etcPhIbqbaSdTGnP1WdiqxBcXEEkg3FiQAUDTslgqoZTkuh05bXOUZzPhLoVD6wIEkX189oDpVT6pOsc7c91p4jovXhuSm52ZoJu0AOkyBLr2i6SQMCFRg5n/cR7bJI5nR/FgR9G/7qf5kpJ0xHqlQAtfdvaDo7BGtwsd9d9JzA6CHzaxgNGblrZYI6U1OR9U1XpO9wg3HiP0WidDaCsRwA4z9+HObnYOwADu5xAJI3MTbRD4boizM6k3E9poDiMgBAd593uOahg+kLxDWiIENALbAbCAo0+NhrzUDRnIguJkkWEew9EfaDQa7oIf+oeP9g/VM3oK7/qHebf7qDOlz/wDGX9FaOlbzuf8AtP5I2CipnQlxJH0iS2Aex58/mU1DoiXZoxE5TlIyGQRzHunp8ac1zntzS653B8jYeSjgOKmmXuY18vjMT2pjTWUUhUEfsW/esP6VIdCqkT13/arB0gqa5Hf0j9E56SVvuP8A6R+iKQUQ/Yyp/wA4ebVE9DKg1rNPkp/tFVN+rf8A0D9FI9Ia33X/ANH6BFRCgjgfADRq53FrxGWC0n4ovflC6LqWx8DJNx2IXKHpLV/5dT+kfooDpFUJvTd/T/ZGwaWdc452izQImwg3jeU/0S2rrEn4ucW00tp3lcl9fO16up5N29Aqz0hfPwVB3pWh0zrKjQ5oaRF5mYd6q5lMZTA79RO+nr7Lj/2ifyf8+Sr+v3j7L/VO0FG9xHHCiMxacpaZOrhFhHmQqMLxig6o+lmOeTTAzC5EElvLQjxBXP4zioq/xKbjtrA1B0HeAq2V6QeKgpHPOYG/xc40JSsdHaUTl0Gv3r6ckbTphwnKwzzaCuMZxtwM9W+eYH9kx6QVdOrf6f8AymmiXFmt0i4R1zqeVzWRmBhmswRoRy91nt6IOJ/j+WT/AO1U3j9b/lu/p/8AlS/aCt9xw8o/JH2gostq9EwxrnuxEBok9mdOXauTyU29EmmIrOMiZDWwNLG+t1n4/jFWpTcxweA4QYMGD5KvD8TqsYGw8gNDQS4zARsOgnC8Cw9RzWNxDy59PrW9kfBOXnrNo7ii6XQlgn99UInkFiUuJBhDmU8rmtLGkGCGkklo5CSTHeiP2lq/eP8AUf1RsDXoK4h0ew1I021KlUkkBsNFszg0TcGL9/wnuBHx/CcLTLmOfWDm0jV2gicrRMGCSQYIsChKvF8xDjTYSDIJuZJmZmxm6erxY3BZTgiCNRHqjYDXwHRrC1Gy2pVdIBEgAwRMjs3G06WPJE1uhWGe2D1t9xlBt35bLm28Xc27Q1tgLWsNBbYKTOkFQfaHqUbBRt/sBhbya0f6xp/QuhrYWREm0ASR+i4R3H6nMepUH8Zqnl7pWgo71lKBH/kkuEHEqnNvz5JJ2FGY9xuhHSrqjxM7zKGqOlZDJUiZSa7tXAPiotqlQDrlF7AXE30hO1VmonFQoVgGU3kozCvusylUuiKT762WiYjoKSdyEwlgP8q3EP8ABSp7mjjsGUEdQWNQdmEHu375WrQfdEpbBFBjhzVbG3T4qkKjS0zB5EtOvMXVbqDXOYTcsMtubGI81EJbFzjua9KjI09BKVSgBt7QraGHbUaabs0O1yuLTa+oNtFHFcKpPrAEEF+VriHEOgRuDOwuuZz3o10bWCVqfcqer7kTiaYpk02kww5RJJMCwkm5KAoYcNIIc6A3KGlxI9Ofet4N0YzSsKZhgdVd9AAvkPomoOK0KHD6YoPYAQHyXS5x1EHtEkjyhYZJtSNscU0Z5YOXsgK2uy0g2BlGgEak6eKAqm63xyMskQd7LISo1HYl1kC91lWvyGkErBUh0CIHmmxLu9UNfe5WurYyrcqxrRaNAPfvWe8laXEHDKDPd4x/lZJKzUrQSW4ziU1RxUXuUXOTsRawp1W1ycuRYFgVpJhDhyszJAEtNkypa9JAFDnKlxSSQIiEoukkgCQCm1qSSaAvosPzZGUW+fjf8kklouBGph9NpVtQJJLPybPgjQb3LSw+qSSJcCiHqLWmUkljDg1ma/DnXHPbZT4lV7QIOkeHdCSSwa+81T+0zK9Qkkne6qaUkl0w4OeYRQctqm52SSYHd/hJJc2blHRh4M2qDOvz6LOri6SS2xvYzmDYwIUi2vz6JJJrdCezM/E7z5IIgJJLZcGL5KMTpCz3OSSUoUipxUSEkkySTUinSTEJsqxJJAx58UkkkAf/2Q==");
        mNames.add("United States of America");

        initRecyclerView();
    }

    private void initRecyclerView(){
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this,mNames,mImageUrls);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_map) {
            Intent i = new Intent(this, MapsActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_places) {
            Intent i = new Intent(this, searchActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
